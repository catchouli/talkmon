'use strict';

// Character class
// Sets up a sprite and event network for an ingame character
// The input signals can be set up differently for local, remote, and npc players
function Character(game) {
  this.game = game;
  this.create();
}

Character.prototype.create = function() {
  this.playerSprite = this.createPlayerSprite();
  this.createEventNetwork();
}

Character.prototype.createPlayerSprite = function() {
  let phaser = this.game.phaser;
  let physics = phaser.physics;

  // Create player sprite
  let playerSprite = phaser.add.sprite(phaser.world.centerX, phaser.world.centerY, 'leafgreen_girl_walk');
  playerSprite.anchor.set(0.5);
  playerSprite.animations.add('walk_down', [0,1,2]);
  playerSprite.animations.add('walk_up', [3,4,5]);
  playerSprite.animations.add('walk_left', [6,7,8]);
  playerSprite.animations.play('walk_down', 0, true);
  physics.arcade.enable(playerSprite);

  return playerSprite;
}

Character.prototype.createEventNetwork = function() {
  let phaser = this.game.phaser;
  let physics = phaser.physics;
  let input = this.game.inputStreams;

  // Create player movement
  const playerMoveTime = 0.25;
  const playerMoveSpeed = 1 / playerMoveTime; // ^ 1 because it's in tile-space
  const initialPlayerPos = vec2(10, 10);

  // Player input property, represents the player's current directional input
  let playerInput = Kefir.combine([ input.left.toProperty().map(x => x ? vec2(-1,0) : vec2(0,0))
                                  , input.right.toProperty().map(x => x ? vec2(1,0) : vec2(0,0))
                                  , input.up.toProperty().map(x => x ? vec2(0,-1) : vec2(0,0))
                                  , input.down.toProperty().map(x => x ? vec2(0,1) : vec2(0,0))
                                  ], (a, b, c, d) => vec2().add(a).add(b).add(c).add(d));

  // An observable that fip flops back and forth between each movement
  // Used to alternate diagonal movements
  let preferredAxis = playerInput.combine(input.tick).throttle(playerMoveTime).scan((b, _) => !b, false);

  // A function which limits a direction vector to one direction depending on the value of axis
  let limitDirection = (dir, axis) => dir.x == 0 || dir.y == 0 ? dir : vec2(axis?dir.x:0, !axis?dir.y:0);

  // Create movement direction property
  let playerMovement = playerInput.combine(input.tick, a => a)
                                  // Filter zeroes or they'll cause the stream to be throttled
                                  .filter(a => a.x != 0 || a.y != 0)
                                  // Throttle to whatever rate we can move at
                                  .throttle(playerMoveTime * 1000, {trailing: false})
                                  // Merge 0 inputs back in so we know when we've stopped
                                  .merge(playerInput.throttle(playerMoveTime*1000).filter(a => a.x == 0 && a.y == 0))
                                  // Limit to one direction, which flip flops back and forth each movement
                                  .zip(preferredAxis, limitDirection).log();

  // The player position property, holds the players *actual* tile position
  // This is activated as soon as they begin to move to a tile, and then their
  // sprite is interpolated to produce smooth movement
  let playerPosition = playerMovement.filter(a => a.x != 0 || a.y != 0)
                                     .scan((pos, vel) => (pos.clone().add(vel)), initialPlayerPos);

  // A property that goes to 0 when the player moves and otherwise counts up to 1
  let movementInterpolant = Kefir.merge([
    // A stream of functions that emit 0 when the player moves
    playerMovement.filter(a => a.x != 0 || a.y != 0).map(() => (() => 0)),
    // A stream of functions that add the time delta to a value when the game ticks
    input.tick.map(dt => (t => t + dt * playerMoveSpeed))
  ]).scan((t, f) => f(t), 0).filter(x => x < 1);

  // Interpolate player position
  let playerDisplayPos = Kefir.constant([initialPlayerPos,initialPlayerPos])
                              .merge(playerPosition.diff())
                              .combine(movementInterpolant, (pos, t) =>
                                vec2(pos[0].x, pos[0].y).mix(vec2(pos[1].x, pos[1].y), t) );

  // Velocity property
  let playerVelocity = playerDisplayPos.diff((a,b) => (a.clone().subtract(b))).toProperty();

  // Update the sprite's position to the smoothed display pos
  playerDisplayPos.onValue(this.updatePosition.bind(this));

  // Update the player's animation
  playerMovement.skipDuplicates().onValue(this.updateAnimation.bind(this));
}

Character.prototype.updatePosition = function(pos) {
  this.playerSprite.x = pos.x * 32;
  this.playerSprite.y = pos.y * 32;
}

Character.prototype.updateAnimation = function(dir) {
  const animFramerate = 5;

  let sprite = this.playerSprite;
  let animations = sprite.animations;
  let currentAnim = animations.currentAnim;

  // Set animation speed
  if (dir.x == 0 && dir.y == 0) {
    currentAnim.stop();
    currentAnim.frame = 1;
  }
  else if (dir.x < 0) {
    sprite.play('walk_left', animFramerate, true);
    sprite.scale.set(1, 1);
  }
  else if (dir.x > 0) {
    sprite.play('walk_left', animFramerate, true);
    sprite.scale.set(-1, 1);
  }
  else if (dir.y < 0) {
    sprite.play('walk_up', animFramerate, true);
    sprite.scale.set(1, 1);
  }
  else if (dir.y > 0) {
    sprite.play('walk_down', animFramerate, true);
    sprite.scale.set(1, 1);
  }
}
