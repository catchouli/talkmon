let vec2 = Victor;

function clamp(min, max, val) {
  return Math.min(Math.max(min, val), max)
}

function lerp(min, max, val) {
  return (1-val)*min + val*max;
}
