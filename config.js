// listen ports - 0 = dont listen on this protocol
const httpPort = 13003;
const httpsPort = 0;

// privkey and full chain for ssl
const privkey = '';
const fullchain = '';

// whether to instruct clients to reload when the server exits
// this can be used to iterate client development with nodemon
// requires SIGUSR2 so might not work on non-unix platforms
const sendRefreshOnExit = false;

exports.httpPort = httpPort;
exports.httpsPort = httpsPort;
exports.privkey = privkey;
exports.fullchain = fullchain;
exports.sendRefreshOnExit = sendRefreshOnExit;
exports.exports = module.exports;
