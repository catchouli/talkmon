#!/usr/bin/env node

'use strict';

const fs = require('fs');
const express = require('express');
const http = require('http');
const https = require('https');
const ioServer = require('socket.io');

// Load config
const configName = (fs.existsSync('config.local.js') ? './config.local.js' : './config.js');
console.log('Loading config ' + configName);
const config = require(configName);

// Create express app
var app = express();

// Create socketio server
const io = new ioServer();

// Start http server
if (config.httpPort != 0) {
  // Create http server
  let webServer = http.createServer(app);

  // Set up listen ports
  webServer.listen(config.httpPort, function() {
    console.log('http listening on *:' + config.httpPort);
  });

  // Attach socket.io
  io.attach(webServer);
}

// Start https server
if (config.httpsPort != 0) {
  // Load private key and cert
  let privkey = fs.readFileSync(config.privkey);
  let fullchain = fs.readFileSync(config.fullchain);

  // Create https server
  let webServer = https.createServer({
    key: privkey,
    cert: fullchain,
  }, app);

  // Set up listen ports
  webServer.listen(config.httpsPort, function() {
    console.log('https listening on *:' + config.httpsPort);
  });

  // Attach socket.io
  io.attach(webServer);
}

// Serve files
app.use('/', express.static(__dirname + '/client'));

// Handle connections and store connected sockets
let connections = {};
io.on('connect', function(socket) {
  console.log('got new connection from ' + socket.id);
  connections[socket.id] = socket;

  socket.on('disconnect', function() {
    delete connections[socket.id];
  });
});

// Auto refresh client when exiting
// This way, you can use nodemon to iterate over the client code easily
if (config.sendRefreshOnExit) {
  // Handle sigint and tell clients to refresh
  process.once('SIGUSR2', function() {
    console.log('Exiting. Refreshing clients.');

    // Tell all clients to refresh
    let keys = Object.keys(connections);
    for (let i = 0; i < keys.length; ++i) {
      connections[keys[i]].emit('refresh');
    }

    // Rethrow this signal so the process can exit
    process.kill(process.pid, 'SIGUSR2');
  });
}
